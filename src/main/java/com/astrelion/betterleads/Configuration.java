package com.astrelion.betterleads;

import com.astrelion.astrelplugin.AstrelConfiguration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

public class Configuration extends AstrelConfiguration
{
    public static boolean UNBREAKABLE_LEADS;
    public static boolean TELEPORT_BEFORE_BREAK;
    public static List<PotionEffect> LEASHED_EFFECTS;
    public static Set<EntityType> LEASHABLE;
    // https://minecraft.fandom.com/wiki/Lead#Leashing_mobs
    public static Set<EntityType> VANILLA_LEASHABLE;

    private final static String[] vanilla = {
        "ALLAY",
        "AXOLOTL",
        "BEE",
        "CAT",
        "CHICKEN",
        "COW",
        "DOLPHIN",
        "DONKEY",
        "FOX",
        "FROG",
        "GLOW_SQUID",
        "GOAT",
        "HOGLIN",
        "HORSE",
        "IRON_GOLEM",
        "LLAMA",
        "MUSHROOM_COW",
        "MULE",
        "OCELOT",
        "PARROT",
        "PIG",
        "POLAR_BEAR",
        "RABBIT",
        "SHEEP",
        "SKELETON_HORSE",
        "SNOWMAN",
        "SQUID",
        "STRIDER",
        "TRADER_LLAMA",
        "WOLF",
        "ZOGLIN",
        "ZOMBIE_HORSE"
    };

    private final BetterLeads plugin;

    public Configuration(BetterLeads plugin)
    {
        super(plugin);
        this.plugin = plugin;

        reload();
    }

    @Override
    public void reload()
    {
        super.reload();

        ENABLE = resource.getBoolean("enable");
        PROMPT_UPDATES = resource.getBoolean("promptUpdates");
        UNBREAKABLE_LEADS = resource.getBoolean("unbreakableLeads");
        TELEPORT_BEFORE_BREAK = resource.getBoolean("teleportBeforeBreak");

        ConfigurationSection effects = resource.getConfigurationSection("leashedEffects");
        LEASHED_EFFECTS = new ArrayList<>();
        for (String key : effects.getKeys(true))
        {
            LEASHED_EFFECTS.add(new PotionEffect(PotionEffectType.getByName(key), Util.secondsToTicks(30), effects.getInt(key) - 1));
        }

        List<String> leashable = resource.getStringList("leashable");
        LEASHABLE = new HashSet<>();
        for (String l : leashable)
        {
            LEASHABLE.add(EntityType.valueOf(l.toUpperCase()));
        }

        // parse list of vanilla mobs by string since older versions won't have some mobs
        VANILLA_LEASHABLE = new HashSet<>();
        for (String entity : vanilla)
        {
            try
            {
                VANILLA_LEASHABLE.add(EntityType.valueOf(entity));
            }
            catch (IllegalArgumentException ignore) {}
        }
    }

    public boolean isLeashable(EntityType entityType)
    {
        return LEASHABLE.contains(entityType);
    }

    public boolean isLeashable(Entity entity)
    {
        return isLeashable(entity.getType());
    }

    public boolean isVanillaLeashable(EntityType entityType)
    {
        return VANILLA_LEASHABLE.contains(entityType);
    }

    public boolean isVanillaLeashable(Entity entity)
    {
        return VANILLA_LEASHABLE.contains(entity.getType());
    }
}

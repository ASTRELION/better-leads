package com.astrelion.betterleads;

import com.astrelion.astrelplugin.AstrelLanguage;

public class Language extends AstrelLanguage
{
    private final BetterLeads plugin;

    public Language(BetterLeads plugin)
    {
        super(plugin);
        this.plugin = plugin;
    }
}

package com.astrelion.betterleads;

import com.astrelion.astrelplugin.AstrelEvents;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityPotionEffectEvent;
import org.bukkit.event.entity.EntityUnleashEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

public class Events extends AstrelEvents
{
    private final BetterLeads plugin;
    private final Configuration configuration;
    private final Permissions permissions;

    public Events(BetterLeads plugin)
    {
        super(plugin);
        this.plugin = plugin;
        this.configuration = plugin.getConfiguration();
        this.permissions = plugin.getPermissions();
    }

    /**
     * PlayerInteractEntityEvent.
     * Handles custom leashing on mobs.
     * @param event the PlayerInteractEntityEvent
     */
    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event)
    {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();
        ItemStack itemStack = player.getEquipment().getItem(event.getHand());

        if (itemStack.getType() == Material.LEAD &&
            entity instanceof LivingEntity livingEntity)
        {
            // unleash
            if (livingEntity.isLeashed() && livingEntity.getLeashHolder() == player)
            {
                if (livingEntity.setLeashHolder(null))
                {
                    event.setCancelled(true);

                    // drop lead item
                    if (player.getGameMode() != GameMode.CREATIVE)
                    {
                        ItemStack lead = new ItemStack(Material.LEAD, 1);
                        livingEntity.getWorld().dropItemNaturally(livingEntity.getLocation(), lead);
                    }
                }
            }
            // leash
            else if (!livingEntity.isLeashed() && permissions.canLeash(player, livingEntity))
            {
                if (livingEntity.setLeashHolder(player))
                {
                    event.setCancelled(true);

                    if (permissions.usesBetterLeads(player))
                    {
                        livingEntity.addPotionEffects(Configuration.LEASHED_EFFECTS);
                    }

                    // consume lead
                    if (player.getGameMode() != GameMode.CREATIVE)
                    {
                        itemStack.setAmount(itemStack.getAmount() - 1);
                    }
                }
            }
        }
    }

    /**
     * PlayerLeashEntityEvent.
     * Handles adding potion effects to leashed mobs.
     * @param event the PlayerLeashEntityEvent
     */
    @EventHandler
    public void onPlayerLeashEntity(PlayerLeashEntityEvent event)
    {
        Player player = event.getPlayer();
        Entity entity = event.getEntity();

        // Add potion effects to leashed mobs
        if (entity instanceof LivingEntity livingEntity && permissions.canLeash(player, livingEntity))
        {
            if (permissions.usesBetterLeads(player))
            {
                livingEntity.addPotionEffects(Configuration.LEASHED_EFFECTS);
            }
        }
        else
        {
            event.setCancelled(true);
        }
    }

    /**
     * EntityUnleashEvent.
     * Handles unbreakable leads and teleporting on break.
     * @param event the EntityUnleashEvent
     */
    @EventHandler
    public void onEntityUnleash(EntityUnleashEvent event)
    {
        EntityUnleashEvent.UnleashReason reason = event.getReason();
        Entity entity = event.getEntity();

        if (entity instanceof LivingEntity livingEntity &&
            livingEntity.isLeashed() &&
            livingEntity.getLeashHolder() instanceof Player player)
        {
            ItemStack mainHand = player.getEquipment().getItemInMainHand();
            ItemStack offHand = player.getEquipment().getItemInOffHand();

            // handle unleash manually with PlayerInteractEntityEvent
            // on vanilla non-leashable mobs, unleash gets called instantly on leash
            // so handle unleash manually instead if player is holding a lead
            if (reason == EntityUnleashEvent.UnleashReason.PLAYER_UNLEASH &&
                (mainHand.getType() == Material.LEAD || offHand.getType() == Material.LEAD))
            {
                event.setCancelled(true);
            }

            // prevent lead from breaking
            if ((Configuration.UNBREAKABLE_LEADS || Configuration.TELEPORT_BEFORE_BREAK) &&
                reason == EntityUnleashEvent.UnleashReason.DISTANCE &&
                permissions.usesBetterLeads(player))
            {
                event.setCancelled(true);

                // teleport leashed to the player if it was going to break the lead
                if (Configuration.TELEPORT_BEFORE_BREAK)
                {
                    livingEntity.teleportAsync(livingEntity.getLeashHolder().getLocation());
                }
            }
        }
    }

    /**
     * PlayerTeleportEvent.
     * Handles teleporting leashed mobs through dimensions.
     * @param event the PlayerTeleportEvent
     */
    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event)
    {
        Player player = event.getPlayer();

        if (!permissions.usesBetterLeads(player)) return;

        for (Entity entity : Util.getLeashedEntities(player))
        {
            LivingEntity livingEntity = (LivingEntity) entity;
            livingEntity.teleportAsync(event.getTo())
                .whenComplete((a, b) -> livingEntity.setLeashHolder(player));
        }
    }

    /**
     * EntityPotionEffectEvent.
     * Handles refreshing potion effects on leashed mobs.
     * @param event the EntityPotionEffectEvent
     */
    @EventHandler
    public void onEntityPotionEffect(EntityPotionEffectEvent event)
    {
        EntityPotionEffectEvent.Cause cause = event.getCause();
        Entity entity = event.getEntity();

        // Refresh potion effects on leashed mobs
        if (cause == EntityPotionEffectEvent.Cause.EXPIRATION &&
            entity instanceof LivingEntity livingEntity &&
            livingEntity.isLeashed() &&
            livingEntity.getLeashHolder() instanceof Player player &&
            permissions.usesBetterLeads(player))
        {
            livingEntity.addPotionEffects(Configuration.LEASHED_EFFECTS);
        }
    }
}
